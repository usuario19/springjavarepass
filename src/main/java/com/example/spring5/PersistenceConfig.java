package com.example.spring5;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration //clase de configuracion
@EnableJpaAuditing//habilitar los campos de auditoria createTime y updateTime y surjan efecto
public class PersistenceConfig {
}
