package com.example.spring5.services;


import com.example.spring5.model.Employee;
import com.example.spring5.repositories.EmployeeRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
//public class EmployeeServiceImpl implements EmployeeService {
public class EmployeeServiceImpl extends GenericServiceImpl<Employee> implements EmployeeService {
    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    protected CrudRepository<Employee, Long> getRepository() {
        return employeeRepository;
    }

    /*

    @Override
    public Set<Employee> getEmployees() {
        Set<Employee> employee= new HashSet<>();
        employeeRepository.findAll().iterator().forEachRemaining(employee::add);
        return employee;
    }*/

}
