package com.example.spring5.services;

import antlr.ASTNULLType;
import com.example.spring5.model.Category;
import com.example.spring5.repositories.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public interface CategoryService extends GenericService<Category>{

    List<Category> find(String code);

}
