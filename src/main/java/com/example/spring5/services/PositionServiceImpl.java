package com.example.spring5.services;

import com.example.spring5.model.Position;
import com.example.spring5.repositories.PositionRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
//public class EmployeeServiceImpl implements EmployeeService {
public class PositionServiceImpl extends GenericServiceImpl<Position> implements PositionService {
    private PositionRepository positionRepository;

    public PositionServiceImpl(PositionRepository positionRepository) {
        this.positionRepository = positionRepository;
    }

    @Override
    protected CrudRepository<Position, Long> getRepository() {
        return positionRepository;
    }

}
