package com.example.spring5.services;

import com.example.spring5.model.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {
}
