package com.example.spring5.services;

import com.example.spring5.model.Position;

public interface PositionService extends GenericService<Position> {
}
