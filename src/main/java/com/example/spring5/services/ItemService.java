package com.example.spring5.services;

import com.example.spring5.model.Item;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface ItemService extends GenericService<Item> {
    void saveImage(Long id, MultipartFile file);
}
