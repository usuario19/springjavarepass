package com.example.spring5.services;

import com.example.spring5.model.SubCategory;
import com.example.spring5.repositories.SubCategoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
//public class EmployeeServiceImpl implements EmployeeService {
public class SubCategoryServiceImpl extends GenericServiceImpl<SubCategory> implements SubCategoryService {
    private SubCategoryRepository subCategoryRepository;

    public SubCategoryServiceImpl(SubCategoryRepository subCategoryRepository) {
        this.subCategoryRepository = subCategoryRepository;
    }

    @Override
    protected CrudRepository<SubCategory, Long> getRepository() {
        return subCategoryRepository;
    }

}
