package com.example.spring5.services;

import com.example.spring5.model.Category;
import com.example.spring5.repositories.CategoryRepository;
import com.microsoft.sqlserver.jdbc.StringUtils;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
///controlador llama al un servicio y ste con mas servicio y solo conun repositorio
public class CategoryServiceImpl extends GenericServiceImpl<Category> implements CategoryService{

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

   /* @Override
    public Set<Category> getCategories() {
        Set<Category> categories= new HashSet<>();
        categoryRepository.findAll().iterator().forEachRemaining(categories::add);
        return categories;
    }

    @Override
    public Category findById(Long id) {

        return categoryRepository.findById(id).get();

    }*/

    @Override
    public List<Category> find(String code) {
        return StringUtils.isEmpty(code)? findAll() : categoryRepository.findByCode(code ).get();
    }

    @Override
    protected CrudRepository<Category, Long> getRepository() {
        return categoryRepository;
    }
}
