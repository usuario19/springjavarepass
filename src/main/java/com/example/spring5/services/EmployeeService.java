package com.example.spring5.services;

import com.example.spring5.model.Employee;

import java.util.Set;

public interface EmployeeService extends GenericService<Employee>{

    //Set<Employee> getEmployees();
}
