package com.example.spring5.services;

import com.example.spring5.model.Contract;
import com.example.spring5.repositories.ContractRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
//public class EmployeeServiceImpl implements EmployeeService {
public class ContractServiceImpl extends GenericServiceImpl<Contract> implements ContractService {
    private ContractRepository contractRepository;

    public ContractServiceImpl(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @Override
    protected CrudRepository<Contract, Long> getRepository() {
        return contractRepository;
    }

}
