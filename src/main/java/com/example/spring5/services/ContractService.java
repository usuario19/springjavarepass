package com.example.spring5.services;

import com.example.spring5.model.Contract;

public interface ContractService extends GenericService<Contract> {
}
