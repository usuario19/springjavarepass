package com.example.spring5.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.spring5.model.Contract;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
