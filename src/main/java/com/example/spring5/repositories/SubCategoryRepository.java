package com.example.spring5.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.spring5.model.SubCategory;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
}
