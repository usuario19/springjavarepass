package com.example.spring5.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.spring5.model.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
