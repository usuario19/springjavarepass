package com.example.spring5.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.spring5.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<List<Category>> findByCode(String code);//findBy.....(String code)->compara el cmpo con esto
}
