package com.example.spring5.repositories;

import org.springframework.data.repository.CrudRepository;
import com.example.spring5.model.Position;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
