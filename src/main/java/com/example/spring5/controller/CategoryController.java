package com.example.spring5.controller;

import com.example.spring5.repositories.CategoryRepository;
import com.microsoft.sqlserver.jdbc.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/categories")
public class CategoryController {
    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    /*@RequestMapping
    public String getCategories(Model model) {
        model.addAttribute("category", categoryRepository.findAll());
        return "categories";//template
    }*/


    @RequestMapping//pide parametro code
    public String getCategories(@RequestParam(value = "code", required = false) String code, Model model) { //puede recibir o no code
        model.addAttribute("categories", StringUtils.isEmpty(code) ? categoryRepository.findAll() : categoryRepository.findByCode(code).get());//SI PARRAMETRO ES NULO si recibe code
        return "categories";
    }

    @RequestMapping("/{id}")//VALOR POR URL POR ESO PathVariable
    public String getCategoriesById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("categories", categoryRepository.findById(id).get()); //findById parte del crud repository
        return "categories";//nombre del template
    }
}

