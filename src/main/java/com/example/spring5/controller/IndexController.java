package com.example.spring5.controller;

import org.springframework.web.bind.annotation.RequestMapping;

public class IndexController {
    @RequestMapping({"","/","/index"})//redirecciona a una vista
    public String getIndexPage(){

        return "index";
    }
}
