package com.example.spring5.controller;

import com.example.spring5.services.PositionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/positions")
public class PositionController {
    private PositionService service;

    public PositionController(PositionService service) {
        this.service = service;
    }

    @RequestMapping
    public String getPositions(Model model) {
        model.addAttribute("positions", service.findAll());
        return "positions";
    }

    @RequestMapping("/{id}")
    public String getPositionsById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("position", service.findById(id));
        return "position";
    }
}    

