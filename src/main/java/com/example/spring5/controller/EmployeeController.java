package com.example.spring5.controller;


import com.example.spring5.services.EmployeeService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;

@Controller//por eso no utiliza new para crear el employeeRepositorio
@RequestMapping("/employees")
public class EmployeeController {
    //private EmployeeRepository employeeRepository;
    private EmployeeService service;

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }
/*public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }*/

    //metodo q busque empleados recibe modelo de spring
    //mostrarlo a la vista creando una ruta metohd get


   /* @RequestMapping
    public String getEmployees(Model model){
        model.addAttribute("employees", employeeRepository.findAll());
        return "employees";//vista a la q mapperara
    }*/

    @RequestMapping
    public String getEmployees(Model model) {
        model.addAttribute("employees", service.findAll());
        return "employees";
    }
    @RequestMapping("/{id}")
    public String getItemsById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("employee", service.findById(id));
        return "employee";
    }
}
