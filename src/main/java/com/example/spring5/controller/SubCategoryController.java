package com.example.spring5.controller;

import com.example.spring5.services.SubCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/subCategories")
public class SubCategoryController {
    private SubCategoryService service;

    public SubCategoryController(SubCategoryService service) {
        this.service = service;
    }

    @RequestMapping
    public String getSubCategorys(Model model) {
        model.addAttribute("subCategories", service.findAll());
        return "subCategories";
    }

    @RequestMapping("/{id}")
    public String getSubCategorysById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("subCategory", service.findById(id));
        return "subCategory";
    }
}    

