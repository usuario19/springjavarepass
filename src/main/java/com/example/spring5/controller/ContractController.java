package com.example.spring5.controller;

import com.example.spring5.services.ContractService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/contracts")
public class ContractController {
    private ContractService service;

    public ContractController(ContractService service) {
        this.service = service;
    }

    @RequestMapping
    public String getContracts(Model model) {
        model.addAttribute("contracts", service.findAll());
        return "contracts";
    }

    @RequestMapping("/{id}")
    public String getContractsById(@PathVariable("id") @NotNull Long id, Model model) {
        model.addAttribute("contract", service.findById(id));
        return "contract";
    }
}    

