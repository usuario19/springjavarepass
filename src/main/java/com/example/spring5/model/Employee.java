package com.example.spring5.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;
@Entity
//al decir que sonentidades se puede utilizar JPA
public class Employee extends ModelBase{

    private String firstName;
    private String lastName;
    //Lazy que se muestre cuando se ingrese a employee o cuando lo requierea
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee", cascade = CascadeType.ALL)
    private Set<Contract> contract = new HashSet<>();//inicializar este manda id a la tabla Contract
    //empleado tiene un contrato
    // set listas evita problemas de duplicados


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Contract> getContract() {
        return contract;
    }

    public void setContract(Set<Contract> contract) {
        this.contract = contract;
    }
}
