package com.example.spring5.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
//es herencia contiene datos mapeados comunues a las clases hijas
//mapeo con una  Clase no con un atributo o var primitivo necesriemente

@EntityListeners(AuditingEntityListener.class)
//cuando quieres hacer auditoria
public class ModelBase {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)//campo autogenerado
    private Long id;
    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP) //solo hora
    @Column(nullable = false, updatable = false)
    private Date createOn;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP) //solo hora
    @Column(insertable = false)
    private Date updateOn;
    @Version
    @Column(nullable = false)
    private Long version;

    public Date getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Date createOn) {
        this.createOn = createOn;
    }

    public Date getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(Date updateOn) {
        this.updateOn = updateOn;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
