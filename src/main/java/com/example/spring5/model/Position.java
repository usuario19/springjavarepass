package com.example.spring5.model;

import javax.persistence.Entity;

@Entity
public class Position extends ModelBase {
    //si no hay constructor de supone por defecto q es el vacio
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
