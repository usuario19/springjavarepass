package com.example.spring5.model;

import org.springframework.ui.Model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Date;
@Entity
public class Contract extends ModelBase {

    @OneToOne(optional = false)//cuando tienes un contrato el id empleado es required
    private Employee employee;
    @OneToOne(optional = false)//solo para una position po esi solo se pone one to one
    private Position position;
    private Date initDate;
    private Date endDate;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
