package com.example.spring5.services;

import com.example.spring5.model.Category;
import com.example.spring5.repositories.CategoryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

public class CategoryServiceImplTest {
    @Mock
    CategoryRepository categoryRepository;
    @InjectMocks
    CategoryServiceImpl categoryServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

   /* @Test
    public void testGetCategories() throws Exception {
        Set<Category> result = categoryServiceImpl.findAll();
        Assert.assertEquals(new HashSet<Category>(Arrays.asList(new Category())), result);
    }*/

    @Test
    @Ignore
    public void testFindById() throws Exception {
        Category result = categoryServiceImpl.findById(Long.valueOf(1));
        Assert.assertEquals(new Category(), result);
    }

    @Test
    @Ignore
    public void testFind() throws Exception {
        when(categoryRepository.findByCode(anyString())).thenReturn(null);

        List<Category> result = categoryServiceImpl.find("code");
        Assert.assertEquals(Arrays.<Category>asList(new Category()), result);
    }
}
